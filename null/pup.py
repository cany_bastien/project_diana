import json

data = []
data.append({
    'name': 'Scott',
    'website': 'stackabuse.com',
    'from': 'Nebraska'
})
data.append({
    'name': 'Larry',
    'website': 'google.com',
    'from': 'Michigan'
})
data.append({
    'name': 'Tim',
    'website': 'apple.com',
    'from': 'Alabama'
})

with open('data.json', 'w') as f:
    json.dump(data, f,sort_keys=True , ensure_ascii=False, indent=4)
