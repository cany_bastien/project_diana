import audiogen
import itertools
import sys

audiogen.sampler.play(
    itertools.cycle(itertools.chain(audiogen.beep(), audiogen.silence(0.5)))
)
