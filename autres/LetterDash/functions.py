def save_records(mot, lettres):
	with open('records','w') as save:
		save.write(str(mot) + ";" + str(lettres))

def load_records():
	with open('records','r') as load:
		return [float(x) for x in load.readline().split(";")]
