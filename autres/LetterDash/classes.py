#coding:utf-8
import pygame
import random
from numpy import where
from pygame.locals import *
pygame.font.init()

class LetterCloud:
	@classmethod
	def init(cls, win):
		cls.win = win
		cls.liste_labels = [Label(win),Label(win)]
		cls.liste_labels[0].pos[0] += cls.liste_labels[0].render.get_rect().w
		cls.liste_labels[1].pos[0] += cls.liste_labels[1].render.get_rect().w//2

	@classmethod
	def verif_text(cls, text):
		for i, lab in enumerate(cls.liste_labels):
			if lab.text == text:
				cls.liste_labels[i].fade()
				return True

	@classmethod
	def pop(cls): # ICI s'occuper des histoires de collisions
		new_lab = Label(cls.win)
		nl_rect = new_lab.render.get_rect()
		nl_rect.x = int(new_lab.pos[0]) 
		nl_rect.y = int(new_lab.pos[1]) 
		
		collision = True
		
		for lab in (l for l in cls.liste_labels if l.pos[0] < 0):
			rect = lab.render.get_rect()
			rect.x = int(lab.pos[0]) 
			rect.y = int(lab.pos[1])

			# ici faire boucler jusqu'à ce qu'il n'y ais plus de collisions
			if nl_rect.colliderect(rect):
				new_lab.pos[1] = lab.pos[1] + lab.render.get_rect().h
				break

		cls.liste_labels.append(new_lab)

	@classmethod
	def fade(cls):
		for l in cls.liste_labels:
			l.fade()


	@classmethod
	def show(cls):
		for label in reversed(cls.liste_labels):
			if label.garbage:
				cls.liste_labels.remove(label)





class TextBar:
	include = [] # contient les caractère possibles
	
	def __init__(self, win):
		self.win = win
		self.color = (100,100,100)
		self.text = ""
		self.validate = []
		self.size = 20

	def clean(self): self.text = ""

	def write_text(self, events):
		for event in events:
			if event.type == KEYDOWN:
				if event.key == K_SPACE:
					if LetterCloud.verif_text(self.text):
						self.validate.append(self.text)
					self.clean()

				elif event.key == K_BACKSPACE and self.text:
					self.text = self.text[:-1]

				elif event.unicode in self.include:
					self.text += event.unicode

	def show(self, events):
		self.write_text(events)
		rendu = Label.fonts[self.size].render(self.text, True, self.color)
		self.win.blit(rendu, [self.win.get_rect().w//2 - rendu.get_rect().w//2, 5])




class Label:
	fonts = []
	for x in range(50):
		fonts.append(pygame.font.SysFont("notoserif", x+1))

	# chargement du dictionnaire
	with open("dico_francais.txt","r") as dico:
		texts = dico.read().split("\n")
		texts = [t for t in texts if " " not in t]
		for mot in texts:
			for lettre in mot:
				if lettre not in TextBar.include:
					TextBar.include.append(lettre)
		
	def __repr__(self):
		return "<"+self.text+">"
	def __str__(self):
		return "<"+self.text+">"

	def __init__(self, win, pos=None, text=None, color=[0,255,0], size=20):
		self.win = win
		self.color = color
		self.exploding = False
		self.fading = False
		self.new_alpha = 255
		self.i_explode = 0
		self.garbage = False
		self.size = size
		
		if text:
			self.text = text
		else:
			self.text = random.choice(Label.texts)

		self.render = Label.fonts[size].render(self.text, True, self.color)
		
		if pos:
			self.pos = pos
		else:
			self.pos = [-self.render.get_rect().w, random.randint(20, win.get_rect().h-30)]


	def show(self):
		if self.exploding:
			try:
				old_w = self.render.get_rect().w
				old_h = self.render.get_rect().h
				self.render = Label.fonts[self.i_explode+20].render(self.text, True, self.color)
				deltax = self.render.get_rect().w - old_w
				deltay = self.render.get_rect().h - old_h
				self.pos[0] -= deltax
				self.pos[1] -= 1


				self.i_explode += 1
			except IndexError:	
				self.garbage = True

		elif self.fading:
			if self.new_alpha <= 0:
				self.garbage = True
			else:
				pygame.surfarray.pixels3d(self.render)[where (pygame.surfarray.pixels_alpha(self.render) > 0)] -= 25
				self.new_alpha -= 25.5

		self.win.blit(self.render, [int(x) for x in self.pos])


	def set_text(self, text):
		self.text = text
		self.render = Label.fonts[self.size].render(text, True, self.color)

	def move(self, speed):
		if not self.exploding:
			self.pos[0] += speed

	def explode(self):
		self.exploding = True

	def fade(self):
		self.fading = True

	def centerize(self,coord=None):
		if coord == 'x':
			self.pos[0] = self.win.get_rect().w//2 - self.render.get_rect().w//2
		elif coord == 'y':
			self.pos[1] = self.win.get_rect().h//2 - self.render.get_rect().h//2
		else:
			self.pos[0] = self.win.get_rect().w//2 - self.render.get_rect().w//2
			self.pos[1] = self.win.get_rect().h//2 - self.render.get_rect().h//2
