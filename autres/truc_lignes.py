import pygame
run = True
pygame.init()
win  = pygame.display.set_mode((0,0), pygame.FULLSCREEN)

pygame.key.set_repeat(200,20)
pygame.mouse.set_visible(False)
controlfps = pygame.time.Clock()

spaceline = 100
width = 10

while run :
	events = pygame.event.get()
	for event in events : 
		if event.type == pygame.QUIT : run = False
		elif event.type == pygame.KEYDOWN : 
			if event.key == pygame.K_RIGHT :
				spaceline += 1
			elif event.key == pygame.K_LEFT :
				spaceline -= 1
			elif event.key == pygame.K_UP :
				width += 1
			elif event.key == pygame.K_DOWN :
				width -= 1
			elif event.key == pygame.K_ESCAPE :
				 run = False


	win.fill((0,0,0))

	""" ICI il faudrat que tu empêche spaceline de devenir = à 0 sinon tu te retrouve avec une division
	par 0 ce qui comme tu le sais est impossible et renvoie une erreur """
	for line in range(int(win.get_rect().w/spaceline) +1):
		pygame.draw.rect(win,(255,0,0),(line*spaceline, 0, width, win.get_rect().h ))

	pygame.display.flip ()
	controlfps.tick (100)